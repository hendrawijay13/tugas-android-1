package com.example.master.tugasandroid1

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class LoginMessageFragment : Fragment() {

    companion object {
        fun newInstance() = LoginMessageFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
                R.layout.login_message_fragment,
                container,
                false
        )

    }
}