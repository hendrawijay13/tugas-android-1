package com.example.master.tugasandroid1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tempelFragment1()
        tempelFragment2()
    }

    private fun tempelFragment1() {
        val fragment = LoginInputFragment.newInstance()

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_1,
                fragment
        )
        fragmentTransaction.commit()
    }
    private fun tempelFragment2() {
        val loginMessageFragment = LoginMessageFragment.newInstance()

        supportFragmentManager.beginTransaction().apply {
            replace(
                    R.id.fragment_container_2,
                    loginMessageFragment
            )
            commit()
        }
    }
}
